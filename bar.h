#pragma once

#include "soundData.h"
#include "note.h"
#include <vector>

class Bar
{
private:
	SoundData* soundData;
	int startTime; //samples
	int endTime;
	std::vector<Note> notes;
public:
	Bar() = delete;
	Bar(int startTime, int endTime, SoundData* soundData) : startTime(startTime), endTime(endTime) { this->soundData = soundData; };
	Bar(int startTime) : startTime(startTime) {};
	void addNote(Note note);
	int getStartTime() { return startTime; };
	std::vector<Note>& getNotes() { return notes; };
};

