#pragma once

#include <vector>
#include <valarray>

class SoundData
{
	private:
		unsigned int BPM;
		float numDuration;
		unsigned short int numChannels;
		unsigned int sampleRate;
		unsigned short int bitsPerSample;
		
	public:
		SoundData() {};
		SoundData(float numDuration, unsigned short int numChannels, unsigned int sampleRate, unsigned short int bitsPerSample, unsigned int BPM) :
			numDuration(numDuration), numChannels(numChannels), sampleRate(sampleRate), bitsPerSample(bitsPerSample), BPM(BPM) 
		{
			samples.resize(sampleRate * numChannels * numDuration);
		};
		float getDuration() { return numDuration; };
		unsigned short int getNumChannels() { return numChannels; };
		unsigned int getSampleRate() { return sampleRate; };
		unsigned short int getBitsPerSample() { return bitsPerSample; };
		unsigned int getDataSize() { return samples.size(); };
		unsigned int getBPM() { return BPM; };
		void setDuration(float numDuration);
		void setNumChannels(unsigned short int numChannels) { this->numChannels = numChannels; };
		void setSampleRate(unsigned int sampleRate) { this->sampleRate = sampleRate; };
		void setBitsPerSample(unsigned short int bitsPerSample) { this->bitsPerSample = bitsPerSample; };
		void setBPM(unsigned int BPM) { this->BPM = BPM; };

		std::valarray<float>* getSamples() { return &samples; };
		void lowpassFilter(float alpha);
		void normalize();
		std::valarray<float> samples;
};