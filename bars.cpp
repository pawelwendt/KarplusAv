#include "bars.h"
#include <iostream>

/**
 * @brief Adds new bar.
 * Creates new Bar on stack and adds it to a vector<Bar> bars.
 * @see Bars::calculateBarStart()
 */
void Bars::addBar()
{
	Bar bar(calculateBarStart(), 2 * calculateBarStart(), soundData);
	bars.push_back(bar);
}

/**
 * @brief Calculate new bar start.
 * Takes inner vector<Bar> size and multiply by samples per bar(calculated using BPM and sample rate).
 * @see Bars::addBar()
 */

int Bars::calculateBarStart()
{
	return bars.size() * (((60.0f / soundData->getBPM()) * soundData->getSampleRate()) * 4);
}

/**
 * @brief Generate sound samples.
 * Looping through std::vector<Note>, which are notes(pitch, duration, start time relative to bar beggining)
 * considering it's duration and position
 * @see Note::generateSample()
 */

void Bars::generatePluck()
{
	for (unsigned int i = 0; i < soundData->getDataSize(); ++i)
	{
		soundData->samples[i] = 0;
		for (auto& bar : bars)
		{
			for (auto& note : bar.getNotes())
			{
				if (note.getStartTime() <= i && i < (note.getDuration() + note.getStartTime()))
					soundData->samples[i] += note.generateSample();
			}
		}
		soundData->samples[i] *= 0.75f;
	}
}

/**
 * @brief bars[] operator
 * @return bar at i-th position if (i < bars.size - 1 && i >= 0). Otherwise nullptr.
 */

Bar * Bars::operator[](int i)
{
	return (bars.size() - 1 < i || bars.size() < 0) ? nullptr : &bars[i];
}
