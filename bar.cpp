#pragma once
#include "bar.h"

/**
 * @brief Adds new Note to a Bar.
 * Changes note start time from relative to begging of the Bar to absolute number of samples.
 * @param note to be added to a Bar.
 * @see Bars::addBar()
 */
void Bar::addNote(Note note)
{
	int startTime = this->startTime + (((60.0f / soundData->getBPM()) * soundData->getSampleRate()) *  note.getStartTime());
	note.setStartTime(startTime);
	notes.push_back(note);
}
