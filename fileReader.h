#pragma once
#include "bar.h"
#include "bars.h"
#include "note.h"
#include "soundData.h"
#include <string>
#include <tixml2ex.h>
#include <fstream>

class FileReader
{
private:
	SoundData* soundData;
	Bars* bars;
	std::string content;

public:
	FileReader() = delete;
	FileReader(SoundData* soundData, Bars* bars) : soundData(soundData), bars(bars) {};
	void readFromFile(const std::string fileName);
	void parseContent();
	~FileReader();
};

