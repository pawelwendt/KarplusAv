#include "../catch.hpp"
#include "../soundData.h"

TEST_CASE("Sound sample size")
{
	SECTION("44.1kHz 1 channel 10s")
	{
		SoundData soundData(10, 1, 44100, 16, 120);
		REQUIRE(soundData.getSamples()->size() == 441000);
	}

	SECTION("44.1kHz 2 channels 10s")
	{
		SoundData soundData(10, 2, 44100, 16, 120);
		REQUIRE(soundData.getSamples()->size() == 882000);
	}

	SECTION("22.05kHz 1 channel 10s")
	{
		SoundData soundData(10, 1, 22050, 16, 120);
		REQUIRE(soundData.getSamples()->size() == 220500);
	}

	SECTION("22.05kHz 2 channel 10s")
	{
		SoundData soundData(10, 2, 22050, 16, 120);
		REQUIRE(soundData.getSamples()->size() == 441000);
	}
}

TEST_CASE("Normalization")
{
	SECTION("8 samples 2, 4, 6, 8, 10, 12, 14, 16")
	{
		SoundData soundData(1, 1, 44100, 16, 120);
		const float expectedResult[9] = {-1.0f, -0.75f, -0.5f, -0.25f, 0.0f, 0.25f, 0.5f, 0.75f, 1.0f};
		for (int i = 0; i < 9; ++i)
		{
			soundData.samples[i] = i * 2.0f;
		}
		soundData.normalize();
		for (int i = 0; i < 9; ++i)
		{
			REQUIRE(soundData.samples[i] == Approx(expectedResult[i]));
		}
	}
}