#include "../catch.hpp"
#include "../bars.h"
#include "../soundData.h"

TEST_CASE("Size of vector<Bar> after 50 bar.addBar()")
{
	SECTION("44.1 kHz") 
	{
		SoundData soundData(7, 1, 44100, 16, 60);
		Bars bars(&soundData);
		for (int i = 0; i < 50; ++i)
		{
			bars.addBar();
		}
		REQUIRE(bars.bars.size() == 50);
	}

	SECTION("22.05 kHz") 
	{
		SoundData soundData(7, 1, 22050, 16, 60);
		Bars bars(&soundData);
		for (int i = 0; i < 50; ++i)
		{
			bars.addBar();
		}
		REQUIRE(bars.bars.size() == 50);
	}
}

TEST_CASE("Start time of a Bar after 1 bar.addBar()")
{
	SECTION("44.1 kHz") 
	{
		SoundData soundData(7, 1, 44100, 16, 120);
		Bars bars(&soundData);
		bars.addBar();
		REQUIRE(bars[0]->getStartTime() == 0);
	}

	SECTION("22.05 kHz") 
	{
		SoundData soundData(7, 1, 22050, 16, 120);
		Bars bars(&soundData);
		bars.addBar();
		REQUIRE(bars[0]->getStartTime() == 0);
	}
}

TEST_CASE("Start time of a Bar after 10 bars.addBar()")
{
	SECTION("44.1 kHz") 
	{
		SoundData soundData(7, 1, 44100, 16, 120);
		Bars bars(&soundData);
		constexpr const int startTime[10] = { 0, 44100 * 2, 44100 * 4 , 44100 * 6 , 44100 * 8 , 44100 * 10 , 44100 * 12 , 44100 * 14 , 44100 * 16 , 44100 * 18 };
		for (int i = 0; i < 10; ++i)
		{
			bars.addBar();
		}

		for (int i = 0; i < 10; ++i)
		{
			REQUIRE(bars[i]->getStartTime() == startTime[i]);
		}
	}

	SECTION("22.05 kHz") 
	{
		SoundData soundData(7, 1, 22050, 16, 120);
		Bars bars(&soundData);
		constexpr const int startTime[10] = { 0, 22050 * 2, 22050 * 4 , 22050 * 6 , 22050 * 8 , 22050 * 10 , 22050 * 12 , 22050 * 14 , 22050 * 16 , 22050 * 18 };
		for (int i = 0; i < 10; ++i)
		{
			bars.addBar();
		}

		for (int i = 0; i < 10; ++i)
		{
			REQUIRE(bars[i]->getStartTime() == startTime[i]);
		}
	}
}

TEST_CASE("Accessing non-existing bar")
{
	SoundData soundData(7, 1, 44100, 16, 120);
	Bars bars(&soundData);
	bars.addBar();
	bars.addBar();
	REQUIRE(bars[2] == nullptr);
}