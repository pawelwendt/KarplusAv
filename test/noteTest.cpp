#include "../catch.hpp"
#include "../note.h"
#include "../soundData.h"

TEST_CASE("Frequency map: A2...A6")
{
	REQUIRE(frequenciesMap["A2" ]== Approx(110.0f));
	REQUIRE(frequenciesMap["A3"] == Approx(220.0f));
	REQUIRE(frequenciesMap["A4"] == Approx(440.0f));
}

TEST_CASE("Frequency of note A4")
{
	SoundData soundData(7, 1, 44100, 16, 60);
	Note note("A4", 1.0f, Duration::FULL, Duration::HALF, &soundData);
	REQUIRE(note.getFrequency() == Approx(440.0f));
}

TEST_CASE("Frequency map: C#2...C#6")
{
	REQUIRE(frequenciesMap["C#3"] == Approx(138.59f));
	REQUIRE(frequenciesMap["C#4"] == Approx(277.18f));
	REQUIRE(frequenciesMap["C#5"] == Approx(554.37f));
}

TEST_CASE("Duration of a full note")
{
	SECTION("44.1kHz")
	{
		SECTION("60BPM")
		{
			SoundData soundData(7, 1, 44100, 16, 60);
			Note note("A4", 1.0f, Duration::FULL, Duration::HALF, &soundData);
			REQUIRE(note.getDuration() == 176400);
		}
		SECTION("120BPM")
		{
			SoundData soundData(7, 1, 44100, 16, 120);
			Note note("A4", 1.0f, Duration::FULL, Duration::HALF, &soundData);
			REQUIRE(note.getDuration() == 88200);
		}
		SECTION("240BPM")
		{
			SoundData soundData(7, 1, 44100, 16, 240);
			Note note("A4", 1.0f, Duration::FULL, Duration::HALF, &soundData);
			REQUIRE(note.getDuration() == 44100);
		}
	}

	SECTION("22.05kHz")
	{
		SECTION("60BPM")
		{
			SoundData soundData(7, 1, 22050, 16, 60);
			Note note("A4", 1.0f, Duration::FULL, Duration::HALF, &soundData);
			REQUIRE(note.getDuration() == 88200);
		}
		SECTION("120BPM")
		{
			SoundData soundData(7, 1, 22050, 16, 120);
			Note note("A4", 1.0f, Duration::FULL, Duration::HALF, &soundData);
			REQUIRE(note.getDuration() == 44100);
		}
		SECTION("240BPM")
		{
			SoundData soundData(7, 1, 22050, 16, 240);
			Note note("A4", 1.0f, Duration::FULL, Duration::HALF, &soundData);
			REQUIRE(note.getDuration() == 22050);
		}
	}
}