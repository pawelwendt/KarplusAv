#include "fileWriter.h"

/**
 * @brief Preparation of WAVE PCM header file.
 * @TODO possibly template this
 */
void WaveFileWriter::prepareHeader()
{
	std::memcpy(fileHeader.chunkID, "RIFF", 4);
	fileHeader.chunkSize = soundData->getDataSize() * fileHeader.numChannels * fileHeader.bitsPerSample / 8 + 36;
	std::memcpy(fileHeader.format, "WAVE", 4);
	
	std::memcpy(fileHeader.subchunk1ID, "fmt ", 4);
	fileHeader.subchunk1Size = 16;
	fileHeader.audioFormat = 1;
	fileHeader.numChannels = soundData->getNumChannels();
	fileHeader.sampleRate = soundData->getSampleRate();
	fileHeader.bitsPerSample = soundData->getBitsPerSample();
	fileHeader.byteRate = fileHeader.sampleRate * fileHeader.numChannels * fileHeader.bitsPerSample / 8;
	fileHeader.blockAlign = fileHeader.numChannels * fileHeader.bitsPerSample / 8;
	
	std::memcpy(fileHeader.subchunk2ID, "data", 4);
	fileHeader.subchunk2Size = soundData->getDataSize() * fileHeader.numChannels * fileHeader.bitsPerSample / 8;
}

/**
 * @brief Converts 32bit float value to 16bit integer required by WAVE PCM.
 */
std::vector<int> WaveFileWriter::convertToInt()
{
	std::vector<int> output(soundData->getDataSize());
	for (unsigned int i = 0; i < soundData->getDataSize(); ++i)
	{
		output[i] = (int)(soundData->samples[i] * 32768.0f);
	}
	return output;
}

/**
 * @brief Writes to a file.
 * @param fileName string containing name of a file. I.e. "foo.wav".
 */
void WaveFileWriter::writeToFile(std::string fileName)
{
	prepareHeader();
	std::vector<int> convertedInput = convertToInt();

	file.open(fileName, std::ios::binary);
	file.write(reinterpret_cast<char*>(&fileHeader), sizeof(WaveFileHeader));
	file.write(reinterpret_cast<char*>(&convertedInput[0]), fileHeader.subchunk2Size);
	file.close();
}

FileWriter::FileWriter()
{
}
