# define the C compiler to use
CC = g++

# define any compile-time flags
CFLAGS = -Wall -O3 -std=c++14

# define the C source files
SRCS = bar.cpp flanger.cpp bars.cpp fileWriter.cpp note.cpp test/*.cpp
OBJS = $(SRCS:.cpp=.o)
MAIN = KarplusStrongTest

#
# The following part of the makefile is generic; it can be used to 
# build any executable just by changing the definitions above and by
# deleting dependencies appended to the file from 'make depend'
#

.PHONY: depend clean

all: $(MAIN)

$(MAIN): $(OBJS) 
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

.cpp.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

clean:
	$(RM) *.o *~ $(MAIN)