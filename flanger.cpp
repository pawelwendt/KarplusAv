#include "flanger.h"

/**
 * @brief Performs flanger effect on samples.
 * @param delay in samples.
 * @param sweepFrequency Flanging frequency.
 * @param range flanging amplitude.
 */
void Flanger::flange(float delay, float sweepFrequency, float range)
{
	for (int i = 0; i < soundData->samples.size() - delay - range; ++i)
	{
		soundData->samples[i] += soundData->samples[i + delay + std::round(range * std::sin(2.0f * M_PI * i * sweepFrequency / soundData->getSampleRate()))];
	}
}