#pragma once
#define _USE_MATH_DEFINES
#include "soundData.h"
#include <math.h>


class Flanger
{
private:
	SoundData* soundData;
public:
	Flanger() = delete;
	Flanger(SoundData* soundData) : soundData(soundData) {};
	void flange(float delay, float sweepFrequency, float range);
};

