#include "fileReader.h"

/**
 * @brief Reads file content
 * Content is stored in member variable FileReader::content
 * @param fileName String containing name of a file. I.e. "foo.xml".
 */

void FileReader::readFromFile(const std::string fileName)
{
	std::ifstream ifs(fileName);
	content = std::string((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));
}

/**
 * @brief Parses xml FileReader::content 
 * Using tinyxml2 and tinexml2ex libraries .xml file is loaded into variables.
 * @see FileReader::readFromFile
 */

void FileReader::parseContent()
{
	using namespace tinyxml2;

	try
	{
		auto sheet = load_document(content);

		unsigned int BPM = std::stoi(text(find_element(*sheet, "Music/BPM")));
		unsigned short int numChannels = std::stoi(text(find_element(*sheet, "Music/channelsNumber")));
		unsigned int sampleRate = std::stoi(text(find_element(*sheet, "Music/sampleRate")));
		unsigned short int bitsPerSample = std::stoi(text(find_element(*sheet, "Music/bitsPerSample")));

		soundData->setBPM(BPM);
		soundData->setNumChannels(numChannels);
		soundData->setSampleRate(sampleRate);
		soundData->setBitsPerSample(bitsPerSample);

	
		auto barsElement = find_element(*sheet, "Music/Bars");
		auto barElement = find_element(barsElement, "Bar");
		for (auto const element : barsElement)
		{
			bars->addBar();
			for (auto const noteElement : barElement)
			{
				float duration = std::stof(text(find_element(noteElement, "Duration")));
				std::string pitch = text(find_element(noteElement, "Pitch"));
				float offset = std::stof(text(find_element(noteElement, "Offset")));
				bars->bars.back().addNote(Note(pitch, 0.996f, duration, offset, soundData));
			}
			barElement = barElement->NextSiblingElement();
		}
		soundData->setDuration(std::ceil(bars->getSize() * (60.0f / BPM) * 4.0f)); // 4.0f assume we are in 4/4 for now
	}
	catch (const std::exception& e)
	{
		std::cout << e.what();
	}
}

FileReader::~FileReader()
{
}
