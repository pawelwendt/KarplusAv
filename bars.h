#pragma once

#include "note.h"
#include "bar.h"
#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <random>

class Bars
{
private: 
	SoundData* soundData;
public:
	Bars() = delete;
	Bars(SoundData* soundData) : soundData(soundData) {};
	std::vector<Bar> bars; //notes
	void addBar();
	int calculateBarStart();
	void generatePluck();
	void setSoundData(SoundData* soundData) { this->soundData = soundData; };
	int getSize() { return bars.size(); };
	Bar* operator[](int i);
};