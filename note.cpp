#include "note.h"

/**
 * @brief Generate note's sample.
 * It's done by averaging two neighbouring samples and applying damping factor.
 * @return New sample.
 */

float Note::generateSample()
{
	float returnValue = buffer[index];
	float value = (buffer[index] + buffer[(index + 1) % buffer.size()]) * 0.5f * feedback;
	buffer[index] = value;
	index = (index + 1) % buffer.size();
	return returnValue;
}