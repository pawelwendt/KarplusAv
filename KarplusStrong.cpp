// KarplusStrong.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "bars.h"
#include "soundData.h"
#include "fileWriter.h"
#include "bar.h"
#include "fileReader.h"
#include "flanger.h"
#include "MoogLadders.h"
#include <iostream>

int main(int argc, char* argv[])
{
	/*if (argc < 3)
	{
		std::cerr << "Give at least 2 argument.\n";
		return -1;
	}*/

	SoundData soundData;
	Bars bars(&soundData);

	FileReader reader(&soundData, &bars);
	reader.readFromFile("twinkle.xml");
	reader.parseContent();

	bars.generatePluck();

	MoogLadders moog(&soundData);
	moog.setCutoff(900.0f);
	moog.setResonance(0.3f);
	moog.apply();

	Flanger flanger(&soundData);
	flanger.flange(12.0f, 7.0f, 7.0f);

	soundData.normalize();
	WaveFileWriter fileWriter(&soundData);
	fileWriter.writeToFile("twinkle.wav");

	return 0;
}

