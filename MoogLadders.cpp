#include "MoogLadders.h"

void MoogLadders::setCutoff(float cutoff)
{
	this->cutoff = 2.0 * cutoff / soundData->getSampleRate();

	p = this->cutoff * (1.8 - 0.8 * this->cutoff);
	k = 2.0 * std::sin(this->cutoff * M_PI * 0.5) - 1.0;
	t1 = (1.0 - p) * 1.386249;
	t2 = 12.0 + t1 * t1;

	setResonance(resonance);
}

void MoogLadders::apply()
{
	for (int i = 0; i < soundData->samples.size(); ++i)
	{
		float x = soundData->samples[i] - resonance * stage[3];

		// Four cascaded one-pole filters (bilinear transform)
		stage[0] = x * p + delay[0] * p - k * stage[0];
		stage[1] = stage[0] * p + delay[1] * p - k * stage[1];
		stage[2] = stage[1] * p + delay[2] * p - k * stage[2];
		stage[3] = stage[2] * p + delay[3] * p - k * stage[3];

		// Clipping band-limited sigmoid
		stage[3] -= (stage[3] * stage[3] * stage[3]) / 6.0;

		delay[0] = x;
		delay[1] = stage[0];
		delay[2] = stage[1];
		delay[3] = stage[2];

		soundData->samples[i] = stage[3];
	}
}
