#pragma once
#define _USE_MATH_DEFINES
#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <random>
#include <cmath>
#include "soundData.h"

static std::map<std::string, float> frequenciesMap = { {"A2", 110.0f}, { "A2#", 116.5409f}, {"B2", 123.4708f}, {"C3", 130.8128f}, {"C#3", 138.5913f},
														{"D3", 146.8324f}, {"D#3", 155.5635f}, {"E3", 164.8138f}, {"F3", 174.6141f}, {"F#3", 184.9972f}, {"G3",195.9977f}, {"G#3", 207.6523f},
														{"A3", 110.0f * 2.0f}, { "A3#", 116.5409f * 2.0f}, {"B3", 123.4708f * 2.0f}, {"C4", 130.8128f * 2.0f}, {"C#4", 138.5913f * 2.0f},
														{"D4", 146.8324f * 2.0f}, {"D#4", 155.5635f * 2.0f}, {"E4", 164.8138f * 2.0f}, {"F4", 174.6141f * 2.0f}, {"F#4", 184.9972f * 2.0f}, {"G4",195.9977f * 2.0f}, {"G#4", 207.6523f * 2.0f},
														{"A4", 110.0f * 4.0f}, { "A4#", 116.5409f * 4.0f}, {"B4", 123.4708f * 4.0f}, {"C5", 130.8128f * 4.0f}, {"C#5", 138.5913f * 4.0f},
														{"D5", 146.8324f * 4.0f}, {"D#5", 155.5635f * 4.0f}, {"E5", 164.8138 * 4.0f}, {"F5", 174.6141f * 4.0f}, {"F#5", 184.9972f * 4.0f}, {"G5",195.9977f * 4.0f}, {"G#5", 207.6523f * 4.0f}
													 };

namespace Duration
{
	float const FULL = 4.0f;
	float const HALF = 2.0f;
	float const QUARTER = 1.0f;
	float const EIGHTH = 0.5f;
	float const SIXTEENTH = 0.25f;
	float const THIRTYSECOND = 0.125f;
	float const SIXTYFOURTH = 0.00625f;
};

namespace Offset
{
	float const FULL = 4.0f;
	float const HALF = 2.0f;
	float const QUARTER = 1.0f;
	float const EIGHTH = 0.5f;
	float const SIXTEENTH = 0.25f;
	float const THIRTYSECOND = 0.125f;
	float const SIXTYFOURTH = 0.00625f;
	float const BEGGINING = 0.0f;
};

namespace RandomHelper
{
	static std::random_device randomDevice;
	static std::mt19937 mt(randomDevice());
	static std::uniform_real_distribution<float> random(-1.0f, 1.0f);
};

class Note
{
private:
	float frequency;
	float feedback;
	std::vector<float> buffer;
	float duration; //in samples
	float startTime;
	SoundData* soundData;
public:
	int index;
	Note() = delete;
	Note(std::string frequency, float feedback, float duration, float startTime, SoundData* soundData) : feedback(feedback), duration(duration), startTime(startTime), soundData(soundData)
	{
		this->frequency = frequenciesMap[frequency];
		this->duration = (int)(((60.0f / soundData->getBPM()) * soundData->getSampleRate()) * duration);
		this->index = 0;
		buffer.resize((int)(std::ceil(float(soundData->getSampleRate()) / this->frequency)));
		for (unsigned int i = 0; i < buffer.size(); ++i)
		{
			buffer[i] = RandomHelper::random(RandomHelper::mt);
		}
	};
	float generateSample();
	float getDuration() { return duration; };
	float getStartTime() { return startTime; };
	void setStartTime(float startTime) { this->startTime = startTime; };
	float getFrequency() { return frequency; };
};