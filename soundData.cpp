#include "soundData.h"

/**
 * @brief set SoundData::numDuration and recalculate number of sound samples.
 * @param numDuration Duration to be set.
 */

void SoundData::setDuration(float numDuration)
{
	this->numDuration = numDuration;
	samples.resize(sampleRate * numChannels * numDuration);
}

/**
 * @brief Simulation of infinite impulse response first order low-pass filter.
 * Averages two samples and multiplies by an alpha factor.
 * @param alpha cutoff ratio.
 */

void SoundData::lowpassFilter(float alpha)
{
	float newSample = 0.0f;
	samples[0] *= alpha;
	for (unsigned int i = 1; i < this->getDataSize(); ++i)
	{
		newSample = samples[i - 1] - (alpha * (samples[i - 1] - samples[i]));
		samples[i] = newSample;
	}
}

/**
 * @brief Normalizes audio samples.
 * First brings values to [0; 1] range, then scales to [-1; 1].
 */

void SoundData::normalize()
{
	float max = samples.max();
	float min = samples.min();

	for (auto& sample : samples)
	{
		sample = (((sample - min) / (max - min)) * 2.0f) - 1.0f;
	}
}
