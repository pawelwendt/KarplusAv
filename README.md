Doxygen: https://pawelwendt.gitlab.io/KarplusAv/
```
Usage: ./KarplusStrong.exe <input.xml> <output.wav>
```
Current restrictions:
-assumed time signature is 4/4
-mono only

.xml format:

```
<Duration> - 1.0 quarter, 2.0 half, 4.0 whole, 0.5 eight etc.
<Pitch> - supported A2 - G#5 (notation without flats) 
<Offset> - time from the beginning of <Bar> in note duration (eg. 1.0 quarter, 2.0 half, 4.0 whole, 0.5 eight etc.)

<?xml version="1.0" ?>
<Music>
    <title>Twinkle, twinkle</title>
    <channelsNumber>1</channelsNumber>
    <sampleRate>44100</sampleRate>
    <bitsPerSample>16</bitsPerSample>
    <BPM>180</BPM>
    <Bars>
        <Bar>
            <Note>
                <Duration>1.0</Duration>
                <Pitch>G3</Pitch>
                <Offset>0.0</Offset>
            </Note>
                <Note>
                <Duration>1.0</Duration>
                <Pitch>G3</Pitch>
                <Offset>1.0</Offset>
            </Note>
            <Note>
                <Duration>1.0</Duration>
                <Pitch>D4</Pitch>
                <Offset>2.0</Offset>
            </Note>
            <Note>
                <Duration>1.0</Duration>
                <Pitch>D4</Pitch>
                <Offset>3.0</Offset>
            </Note>
        </Bar>
        <Bar>
            <Note>
                <Duration>1.0</Duration>
                <Pitch>E4</Pitch>
                <Offset>0.0</Offset>
            </Note>
            <Note>
                <Duration>1.0</Duration>
                <Pitch>E4</Pitch>
                <Offset>1.0</Offset>
            </Note>
            <Note>
                <Duration>2.0</Duration>
                <Pitch>D4</Pitch>
                <Offset>2.0</Offset>
            </Note>
        </Bar>
        <Bar>
            <Note>
                <Duration>1.0</Duration>
                <Pitch>G3</Pitch>
                <Offset>0.0</Offset>
            </Note>
                <Note>
                <Duration>1.0</Duration>
                <Pitch>G3</Pitch>
                <Offset>1.0</Offset>
            </Note>
            <Note>
                <Duration>1.0</Duration>
                <Pitch>D4</Pitch>
                <Offset>2.0</Offset>
            </Note>
            <Note>
                <Duration>1.0</Duration>
                <Pitch>D4</Pitch>
                <Offset>3.0</Offset>
            </Note>
        </Bar>
    </Bars>
</Music>
```