#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include "soundData.h"
class MoogLadders
{
private:
	SoundData* soundData;
	double stage[4] = { 0 };
	double delay[4] = { 0 };

	double p;
	double k;
	double t1;
	double t2;

	float cutoff;
	float resonance;

public:
	MoogLadders() = delete;
	MoogLadders(SoundData* soundData) : soundData(soundData) {};
	void setResonance(float resonance) { this->resonance = resonance * (t2 + 6.0 * t1) / (t2 - 6.0 * t1); };
	void setCutoff(float cutoff);
	void apply();
};

