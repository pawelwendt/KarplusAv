#pragma once

#include "soundData.h"
#include <fstream>
#include <cstring>

class FileWriter
{
	protected:
		std::ofstream file;
		std::string fileName;
		SoundData* soundData;
		virtual void prepareHeader() = 0;
	public:
		FileWriter();
		virtual void writeToFile(std::string fileName) = 0;
};

class WaveFileWriter : FileWriter
{
	private:
		struct WaveFileHeader
		{
			public:
				unsigned char chunkID[4]; //"RIFF"
				unsigned int chunkSize;
				unsigned char format[4]; //"WAVE" 
				
				unsigned char subchunk1ID[4]; //"fmt "
				unsigned int subchunk1Size;
				unsigned short int audioFormat;
				unsigned short int numChannels;
				unsigned int sampleRate;
				unsigned int byteRate;
				unsigned short int blockAlign;
				unsigned short int bitsPerSample;
				
				unsigned char subchunk2ID[4]; //"data"
				unsigned int subchunk2Size;
		};
		WaveFileHeader fileHeader;
		void prepareHeader() override;
		std::vector<int> convertToInt();
		
	public:
		WaveFileWriter() = delete;
		WaveFileWriter(SoundData* soundData) { this->soundData = soundData; };
		void writeToFile(std::string fileName) override;
};
